trigger CreateAccountOrder on Account (before insert, before update, before delete, after delete) {
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            List<Rule__c> rules = [SELECT Rule__c.ID
                                   FROM Rule__c WHERE Rule__c.ObjectApiName__c = 'Account'
                                  		AND Rule__c.Type__c LIKE '%Insert%']; 
            if (rules.size() > 0) {
               // Will Call function from Apex Class
        		// CreateOrdersForRule          
            }        	
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
        	List<Rule__c> rules = [SELECT Rule__c.ID
                                   FROM Rule__c WHERE Rule__c.ObjectApiName__c = 'Account'
                                  		AND Rule__c.Type__c LIKE '%Update%']; 
            if (rules.size() > 0) {
               // Will Call function from Apex Class
        	   // CreateOrdersForRule          
            } 
        }
    } else if (Trigger.isDelete) {
        if (Trigger.isAfter) {
        	List<Rule__c> rules = [SELECT Rule__c.ID
                                   FROM Rule__c WHERE Rule__c.ObjectApiName__c = 'Account'
                                  		AND Rule__c.Type__c LIKE '%Delete%']; 
            if (rules.size() > 0) {
               // Will Call function from Apex Class
        		// CreateOrdersForRule          
            }         
        } else if (Trigger.isBefore) {
             for (Account a : [SELECT Id FROM Account
                     WHERE Id IN (SELECT AccountId FROM Opportunity) AND
                     Id IN :Trigger.old]) {
        		Trigger.oldMap.get(a.Id).addError('Cannot delete account with related opportunities.');
    		}
        }
    }
}