@ECHO OFF
SETLOCAL EnableDelayedExpansion

rem Define Constant Values
SET INIT_PATH=%cd%
SET TEMP_PATH=%INIT_PATH%\temp
SET FRONTEND_PATH=%INIT_PATH%\frontend
SET SFDX_OPTIONS=-w -1
SET REPLACER_CONFIG=

rem Devhub name
SET DEVHUB=DevHub

rem Scratch org config
SET SCRATCH_CONFIG=%INIT_PATH%\config\project-scratch-def.json

rem Check Exec command
IF NOT "%1"=="" (
	rem Set command
	SET EXEC_COMMAND=%1
	SET EXEC_PARAM=%2
	GOTO EXEC
)

rem Input command
SET /P EXEC_COMMAND=Build command: 
IF %EXEC_COMMAND%==0 GOTO END
SET /P EXEC_PARAM=Build param: 
IF %EXEC_PARAM%==0 GOTO END

rem EXEC Unit start
:EXEC

rem EXEC commands evaluation

rem Deploy commands
IF %EXEC_COMMAND%==deploy GOTO DEPLOY
IF %EXEC_COMMAND%==deploy-validate GOTO DEPLOY_VALIDATE
IF %EXEC_COMMAND%==deploy-validate-release GOTO DEPLOY_VALIDATE_RELEASE
IF %EXEC_COMMAND%==deploy-release GOTO DEPLOY_RELEASE
IF %EXEC_COMMAND%==deploy-frontend GOTO DEPLOY_FRONTEND

rem Org commands
IF %EXEC_COMMAND%==org-login GOTO ORG_LOGIN
IF %EXEC_COMMAND%==org-open GOTO ORG_OPEN
IF %EXEC_COMMAND%==org-list GOTO ORG_LIST

rem Scratch commands
IF %EXEC_COMMAND%==scratch-create GOTO SCRATCH_CREATE
IF %EXEC_COMMAND%==scratch-delete GOTO SCRATCH_DELETE
IF %EXEC_COMMAND%==scratch-password GOTO SCRATCH_PASSWORD
IF %EXEC_COMMAND%==scratch-push GOTO SCRATCH_PUSH
IF %EXEC_COMMAND%==scratch-pull GOTO SCRATCH_PULL
IF %EXEC_COMMAND%==scratch-status GOTO SCRATCH_STATUS

GOTO END

rem EXEC commands implementation

rem Deploy implementation
:DEPLOY_RELEASE
SET SFDX_OPTIONS=-w -1 -l RunLocalTests
SET REPLACER_CONFIG=%INIT_PATH%\config\replacer-config_release.ini
:DEPLOY
ECHO.
IF EXIST %TEMP_PATH% rmdir %TEMP_PATH% /s /q
CALL sfdx force:source:convert -d %TEMP_PATH%
rem Call replacer
IF NOT "%REPLACER_CONFIG%"=="" (
	ECHO.
	ECHO Replace tokens in code
	FOR /F "delims== tokens=1,2" %%i IN (%REPLACER_CONFIG%) DO (
		CALL %INIT_PATH%\bin\replacer "%%i" "%%j" %TEMP_PATH%
	)
	ECHO.
)
rem Deploy code
CALL sfdx force:mdapi:deploy -d %TEMP_PATH% -u %EXEC_PARAM% %SFDX_OPTIONS%
IF EXIST %TEMP_PATH% rmdir %TEMP_PATH% /s /q
ECHO.
ECHO Going to deploy frontend...
PAUSE

:DEPLOY_FRONTEND
ECHO.
IF EXIST %FRONTEND_PATH% (
	CD /D %FRONTEND_PATH%
	CALL install
	CALL build
	CALL sfdx force:mdapi:deploy -d %FRONTEND_PATH%\target\src -u %EXEC_PARAM% -w -1
	CALL clean
	CD /D %INIT_PATH%
	ECHO.
)
GOTO END

:DEPLOY_VALIDATE_RELEASE
SET REPLACER_CONFIG=%INIT_PATH%\config\replacer-config_release.ini
:DEPLOY_VALIDATE
SET SFDX_OPTIONS=-w -1 -l RunLocalTests -c
ECHO.
IF EXIST %TEMP_PATH% rmdir %TEMP_PATH% /s /q
CALL sfdx force:source:convert -d %TEMP_PATH%
rem Call replacer
IF NOT "%REPLACER_CONFIG%"=="" (
	ECHO.
	ECHO Replace tokens in code
	FOR /F "delims== tokens=1,2" %%i IN (%REPLACER_CONFIG%) DO (
		CALL %INIT_PATH%\bin\replacer "%%i" "%%j" %TEMP_PATH%
	)
	ECHO.
)
rem Deploy code
CALL sfdx force:mdapi:deploy -d %TEMP_PATH% -u %EXEC_PARAM% %SFDX_OPTIONS%
IF EXIST %TEMP_PATH% rmdir %TEMP_PATH% /s /q
ECHO.
GOTO END

rem Org implementation
:ORG_LOGIN
ECHO.
CALL sfdx force:auth:web:login -a %EXEC_PARAM%
ECHO.
GOTO END

:ORG_OPEN
ECHO.
CALL sfdx force:org:open -u %EXEC_PARAM%
ECHO.
GOTO END

:ORG_LIST
ECHO.
CALL sfdx force:org:list
ECHO.
GOTO END

rem Scratch implementation
:SCRATCH_CREATE
ECHO.
CALL sfdx force:org:create -d 30 -a %EXEC_PARAM% -s -v %DEVHUB% -f %SCRATCH_CONFIG%
ECHO.
GOTO END

:SCRATCH_DELETE
ECHO.
CALL sfdx force:org:delete -u %EXEC_PARAM% -v %DEVHUB%
ECHO.
GOTO END

:SCRATCH_PASSWORD
ECHO.
CALL sfdx force:user:password:generate -u %EXEC_PARAM% -v %DEVHUB%
ECHO.
GOTO END

:SCRATCH_PUSH
ECHO.
CALL sfdx force:source:push -u %EXEC_PARAM% -f
ECHO.
GOTO END

:SCRATCH_PULL
ECHO.
CALL sfdx force:source:pull -u %EXEC_PARAM% -f
ECHO.
GOTO END

:SCRATCH_STATUS
ECHO.
CALL sfdx force:source:status -u %EXEC_PARAM%
ECHO.
GOTO END

rem Exit
:END
PAUSE